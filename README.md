*NOTE: Amazon has randomized the classnames on video files, rendering this plugin ineffective.*
*It will be updated as soon as a workaround is found.*

## Double Prime Video ##
A pure CSS Chrome and Firefox Add-on that modifies the Amazon Prime Video viewing experience to be more similar to YouTube or Netflix.

**Add-on Locations**

Chrome
https://chrome.google.com/webstore/detail/double-prime-video/fflofpacjgknpfbhboabihefnpkpglkh

Firefox
https://addons.mozilla.org/en-US/firefox/addon/double-prime-video/
